# Major Depressive Disorder Prediction (MDD Prediction)

This repository is a part of Data Mining project (KMITL, 1/2017) 

### Setup

##### Prerequisites

First, install [Polymer CLI](https://github.com/Polymer/polymer-cli) using
[npm](https://www.npmjs.com) (we assume you have pre-installed [node.js](https://nodejs.org)).

    npm install -g polymer-cli

Second, install [Bower](https://bower.io/) using [npm](https://www.npmjs.com)

    npm install -g bower
    
Installing all dependencies

    bower install