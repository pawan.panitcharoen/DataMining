import json
import csv
import os

csvdata = {}
filename = "dataset"
def to_csvdict(val,headername):
    global csvdata
    csvdata.update({headername:val})

def matchdict(dict,source,headername,AddToDict,value):
    for item in dict:
        if source in item['name']:
            if AddToDict:
                to_csvdict(item['val'],headername)
            value.append(item['val'])
    return value

def writecsv(filename):
    fieldnames = []
    for key in csvdata:
        fieldnames.append(key)
    if not os.path.isfile(filename+'.csv'):
        with open(filename+'.csv','w') as csvfile:
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames, lineterminator='\n')
            writer.writeheader()
            writer.writerow(csvdata)
    else:
        with open(filename+'.csv','a') as csvfile:
            writer = csv.DictWriter(csvfile, fieldnames = fieldnames, lineterminator='\n')
            writer.writerow(csvdata)

def read_csv(filename):
    if os.path.isfile(filename + '.csv'):
        raw_csvdata = csv.DictReader(open(filename+'.csv'),)
        headers = raw_csvdata.fieldnames
    return raw_csvdata

def remove_processed_csv(filename):
    if os.path.isfile(filename +'.csv'):
        os.remove(filename +'.csv')

def main():
    remove_processed_csv("output2")
    raw_csvdata = read_csv("แบบสอบถามอาการโรคซึมเศร้า ")
    var = 0
    for item in raw_csvdata:
        bully_on_socialmedia = False
        like_affect_happiness = False
        thinks_ppl_have_good_life = False
        depend_on_ppl_on_socialmedia = False
        compare_with_others = False
        always_change_profilepic = False
        feel_desperate = False
        always_post_funny_status = False
        always_post_trendy_status = False
        print('\n')
        age = item['อายุ ']
        to_csvdict(age,"Age")
        if item['เพศ'] == "หญิง":
            to_csvdict("F","Gender")
        else:
            to_csvdict("M","Gender")
        if item['อาศัยอยู่ในกรุงเทพมหานคร'] == "ใช่":
            to_csvdict("Y","in_bangkok")
        else:
            to_csvdict("N","in_bangkok")
        total_family = int(item['จำนวนสมาชิกพี่น้องในครอบครัว(รวมตัวเอง)'])
        member_number = int(item['เป็นบุตรคนที่'])
        if total_family==member_number and total_family > 1:
            to_csvdict("youngest", "child_number")
        elif total_family > 1 and member_number > 1:
            to_csvdict("middle","child_number")
        elif total_family > 1 and member_number == 1:
            to_csvdict("oldest","child_number")
        else:
            to_csvdict("only_child" , "child_number")
        to_csvdict(item['จำนวนสมาชิกพี่น้องในครอบครัว(รวมตัวเอง)'],"total_family_member")
        if item['เคยมีบุคคลในครอบครัวที่มีเป็นโรคซึมเศร้า'] == "มี":
            to_csvdict("Y","family_member_have_depression")
        else:
            to_csvdict("N","family_member_have_depression")
        if item['สถานภาพสมรสของบิดา-มารดา'] == "อยู่ด้วยกัน":
            to_csvdict("together","parent_marriage")
        elif item['สถานภาพสมรสของบิดา-มารดา'] == "หย่า":
            to_csvdict("divorce", "parent_marriage")
        elif item['สถานภาพสมรสของบิดา-มารดา'] == "บุคคลหนึ่งถึงแก่กรรม":
            to_csvdict("passaway","parent_marriage")
        problem_in_life = item['คุณมักจะประสบปัญหาทางด้านต่างๆ']
        for ans in problem_in_life.split(";"):
            if ans == "การเรียน":
                to_csvdict("Y","study_problem")
            else:
                to_csvdict("N","study_problem")
            if ans == "การทำงาน":
                to_csvdict("Y","work_problem")
            else:
                to_csvdict("N","work_problem")
            if ans == "การเงิน":
                to_csvdict("Y","money_problem")
            else:
                to_csvdict("N","money_problem")
            if ans == "ความรัก":
                to_csvdict("Y","love_problem")
            else:
                to_csvdict("N","love_problem")
        if item['หากคุณพบปัญหาที่ไม่สามารถแก้ได้คุณจะทำอย่างไร'] == "ปรึกษา ครอบครัว,เพื่อน หรือคนใกล้ตัว":
            to_csvdict("talk_with_close_person","deal_with_problem")
        elif item['หากคุณพบปัญหาที่ไม่สามารถแก้ได้คุณจะทำอย่างไร'] == "โพสลงในโซเชียลมีเดีย(Facebook, Twitter และ อื่นๆ)":
            to_csvdict("post_on_social_media","deal_with_problem")
        elif item['หากคุณพบปัญหาที่ไม่สามารถแก้ได้คุณจะทำอย่างไร'] == "เลือกที่จะมองข้าม":
            to_csvdict("ignore","deal_with_problem")
        else:
            to_csvdict("none","deal_with_problem")
        if item['จำนวนเวลาที่ใช้งานโซเชียลมีเดียต่อวัน'] == "น้อยกว่า 1 ชั่วโมงต่อวัน":
            to_csvdict("1_hour","socialmedia_usage")
        elif item['จำนวนเวลาที่ใช้งานโซเชียลมีเดียต่อวัน'] == "1 ถึง 3 ชั่วโมงต่อวัน":
            to_csvdict("1_to_3_hours","socialmedia_usage")
        elif item['จำนวนเวลาที่ใช้งานโซเชียลมีเดียต่อวัน'] == "3 ถึง 6 ชั่วโมงต่อวัน":
            to_csvdict("3_to_6_hours","socialmedia_usage")
        else:
            to_csvdict("more_than_6_hours","socialmedia_usage")
        if item['มีสัตว์เลี้ยงหรือไม่'] == "มี":
            to_csvdict("Y","have_pet")
        else:
            to_csvdict("N","have_pet")
        problem = item['ปัญหาบนโซเชียลมีเดียที่คุณเคยพบเจอ'].split(";")
        for ans in problem:
            if ans == "รู้สึกแย่เมื่อมีคนมาคอมเม้นต์ว่าในโซเชียลมีเดีย":
                bully_on_socialmedia = True
            if ans == "รู้สึกว่าจำนวนยอดไลค์, แชร์ ส่งผลต่อความสุข":
                like_affect_happiness = True
            if ans == "มีความเข้าใจผิดเกี่ยวกับความเป็นจริงที่เป็นสุข ของเพื่อนๆ ในโซเชียลมีเดีย":
                thinks_ppl_have_good_life = True
            if ans == "ตกเป็นเหยื่อทางอารมณ์ของกิจกรรมชีวิตประจำวันของผู้อื่นๆ":
                depend_on_ppl_on_socialmedia = True
            if ans == "มักเปรียบเทียบระดับคุณภาพชีวิตของตนเองกับเพื่อนๆ อยู่เสมอ":
                compare_with_others = True
            if ans == "คุณมักเปลี่ยนรูปโปรไฟล์ของตนเองอยู่เสมอ เพื่อเรียกร้องความสนใจ":
                always_change_profilepic = True
            if ans == "คุณมักเปลี่ยนรูปโปรไฟล์ของตนเองอยู่เสมอ":
                always_change_profilepic = True
            if ans == "รู้สึกกระวนกระวายใจ เมื่อไม่สามารถเช็คข้อความ ข่าวสาร หรือ สถานะของคุณได้เหมือนที่ทำเป็นปกติ":
                feel_desperate = True
            if ans == "คุณมักค้นหาข้อความขำขัน แหลมคม เพื่อโพสต์ข้อความบนโซเซียลมีเดีย":
                always_post_funny_status = True
            if ans == "คุณมักโพสต์บทความต่างๆ ที่บ่งบอกว่าคุณเก่ง คุณเกาะติด หรือเป็นผู้นำ มีความสุขและน่าตลกขบขัน":
                always_post_trendy_status = True
        if bully_on_socialmedia:
            to_csvdict("Y","bully_on_socialmedia")
        if not bully_on_socialmedia:
            to_csvdict("N","bully_on_socialmedia")
        if like_affect_happiness:
            to_csvdict("Y","like_affect_happiness")
        if not like_affect_happiness:
            to_csvdict("N","like_affect_happiness")
        if thinks_ppl_have_good_life:
            to_csvdict("Y","thinks_ppl_have_good_life")
        if not thinks_ppl_have_good_life:
            to_csvdict("N","thinks_ppl_have_good_life")
        if depend_on_ppl_on_socialmedia:
            to_csvdict("Y","depend_on_ppl_on_socialmedia")
        if not depend_on_ppl_on_socialmedia:
            to_csvdict("N","depend_on_ppl_on_socialmedia")
        if compare_with_others:
            to_csvdict("Y","compare_with_others")
        if not compare_with_others:
            to_csvdict("N","compare_with_others")
        if always_change_profilepic:
            to_csvdict("Y","always_change_profilepic")
        if not always_change_profilepic:
            to_csvdict("N", "always_change_profilepic")
        if feel_desperate:
            to_csvdict("Y","feel_desperate")
        if not feel_desperate:
            to_csvdict("N","feel_desperate")
        if always_post_funny_status:
            to_csvdict("Y","always_post_funny_status")
        if not always_post_funny_status:
            to_csvdict("N","always_post_funny_status")
        if always_post_trendy_status:
            to_csvdict("Y","always_post_trendy_status")
        if not always_post_trendy_status:
            to_csvdict("N","always_post_trendy_status")
        if item['มีอาการหงุดหงิด หรือ ก้าวร้าว'] == "นานๆครั้ง (1-2 วันต่อสัปดาห์)":
            to_csvdict("rarely","angry")
        elif item['มีอาการหงุดหงิด หรือ ก้าวร้าว'] == "บ่อยครั้ง (3-4 วันต่อสัปดาห์)":
            to_csvdict("frequent","angry")
        else:
            to_csvdict("always","angry")
        if item['รู้สึกไม่ค่อยมีสมาธิเวลาทำสิ่งต่างๆ'] == "นานๆครั้ง (1-2 วันต่อสัปดาห์)":
            to_csvdict("rarely","concentration")
        elif item['รู้สึกไม่ค่อยมีสมาธิเวลาทำสิ่งต่างๆ'] == "บ่อยครั้ง (3-4 วันต่อสัปดาห์)":
            to_csvdict("frequent","concentration")
        else:
            to_csvdict("always","concentration")
        if item['รู้สึกอ่อนเพลีย'] == "นานๆครั้ง (1-2 วันต่อสัปดาห์)":
            to_csvdict("rarely","tired")
        elif item['รู้สึกอ่อนเพลีย'] == "บ่อยครั้ง (3-4 วันต่อสัปดาห์)":
            to_csvdict("frequent","tired")
        else:
            to_csvdict("always","tired")
        if item['การรับประทานอาหาร'] == "มีหลายวันที่รู้สึกไม่อยากกินอาหาร":
            to_csvdict("dont_want_to_eat","eating")
        elif item['การรับประทานอาหาร'] == "มีบางวันที่ไม่รู้สึกอยากกินอาหาร":
            to_csvdict("sometimes_dont_want_to_eat","eating")
        else:
            to_csvdict("normal","eating")
        if item['การนอน'] == "นอนไม่หลับทุกคืน":
            to_csvdict("cant_sleep","sleeping")
        elif item['การนอน'] == "นอนไม่หลับหลายคืน":
            to_csvdict("sometimes","sleeping")
        else:
            to_csvdict("normal","sleeping")
        if item['ตำหนิตัวเอง'] == "นานๆครั้ง (1-2 วันต่อสัปดาห์)":
            to_csvdict("rarely","blaming_themselves")
        elif item['ตำหนิตัวเอง'] == "บ่อยครั้ง (3-4 วันต่อสัปดาห์)":
            to_csvdict("frequently","blaming_themselves")
        else:
            to_csvdict("always","blaming_themselves")
        if item['ความรู้สึกเศร้า'] == "รู้สึกเศร้านานๆครั้ง":
            to_csvdict("rarely","feeling_sad")
        elif item['ความรู้สึกเศร้า'] == "รู้สึกเศร้าบ่อยครั้ง":
            to_csvdict("frequently","feeling_sad")
        else:
            to_csvdict("always","feeling_sad")
        if item['คุณเป็นโรคซึมเศร้าหรือไม่ '] == "เป็น":
            to_csvdict("Y","depression_disorder")
            var+=1
        else:
            to_csvdict("N","depression_disorder")
        writecsv("output2")
    print("Class Y : "+str(var))


if __name__ == "__main__":
    main()
